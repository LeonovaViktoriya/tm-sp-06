package ru.leonova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.Status;

import java.util.Date;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
    @Modifying
    @Query("UPDATE Project SET name = :name, description = :description, dateStart = :dateStart, dateEnd = :dateEnd, status = :status WHERE projectId = :id")
    void update(@Param("id") final String id, @Param("name") final String name, @Param("description") final String description,
                @Param("dateStart") final Date dateStart, @Param("dateEnd") final Date dateEnd, @Param("status") final Status status);

}
