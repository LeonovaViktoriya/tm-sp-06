package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.repository.IProjectRepository;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private  IProjectRepository iprojectRepository;

    @Override
    public List<Project> findAll() {
        return iprojectRepository.findAll();
    }

    @Override
    public Project findOneById(@NotNull final String projectId) throws Exception {
         if(projectId.isEmpty()) throw new Exception("id project is empty!");
        return iprojectRepository.getOne(projectId);
    }

    @Override
    public void save(@NotNull final Project project) {
        iprojectRepository.save(project);
    }

    @Override
    public void removeProject(@NotNull final Project project) {
        iprojectRepository.delete(project);
    }

    @Override
    public void removeProjectById(@NotNull final String projectId) {
        iprojectRepository.deleteById(projectId);
    }

    @Override
    public void updateProject(@NotNull final String projectId, @NotNull final String projectName, @NotNull final String projectDesc,
                              @NotNull final Date dateStart, @NotNull final Date dateEnd, @NotNull Status status) {
        iprojectRepository.update(projectId, projectName, projectDesc, dateStart, dateEnd, status);
    }
}
