package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface ITaskService {
    @Transactional
    List<Task> findAll();

    Task findOneById(@NotNull String id) throws Exception;

    void removeTask(@NotNull Task task);

    void removeTaskById(@NotNull String id);

    void save(String projectId, @NotNull String name, @NotNull String description, @NotNull String startDate, @NotNull String endDate) throws Exception;

    List<Task> findAllByProjectId(@NotNull String id) throws Exception;

    void removeAllByProjectId(@NotNull String id) throws Exception;

    void updateTask(String taskId, String name, String description, Date dateStart, Date dateEnd, Status status);
}
