package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    Project findOneById(@NotNull String projectId) throws Exception;

    void save(@NotNull Project project);

    void removeProject(@NotNull Project project);

    void removeProjectById(@NotNull String projectId);

    void updateProject(@NotNull String projectId, @NotNull String projectName, @NotNull String projectDesc,
                       @NotNull Date dateStart, @NotNull Date dateEnd, @NotNull Status status);
}
