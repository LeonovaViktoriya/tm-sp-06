package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.repository.ITaskRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    ITaskRepository itaskRepository;

    @Autowired
    IProjectService projectService;

    private SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");

    @Override
    @Transactional
    public List<Task> findAll() {
        return itaskRepository.findAll();
    }

    @Override
    public Task findOneById(@NotNull final String id) throws Exception {
        if(id.isEmpty()) throw new Exception("id task is empty!");
        return itaskRepository.getOne(id);
    }

    @Override
    public void removeTask(@NotNull final Task task) {
        itaskRepository.delete(task);
    }

    @Override
    public void removeTaskById(@NotNull final String taskId) {
        itaskRepository.deleteById(taskId);
    }

    @Override
    public void save(String projectId, @NotNull String name, @NotNull String description, @NotNull String startDate, @NotNull String endDate) throws Exception {
        Task task = new Task();
        if(projectId!=null && !projectId.isEmpty()) {
            Project project = projectService.findOneById(projectId);
            if (project != null) task.setProject(project);
        }
        task.setTaskId(UUID.randomUUID().toString());
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(format.parse(startDate));
        task.setDateEnd(format.parse(endDate));
        task.setDateSystem(new Date());
        task.setStatus(Status.PLANNED);
        itaskRepository.save(task);
    }

    @Override
    public List<Task> findAllByProjectId(@NotNull final String id) throws Exception {
        if(id.isEmpty()) throw new Exception("id project is empty!");
        return itaskRepository.findAllByProject_ProjectId(id);
    }


    @Override
    public void removeAllByProjectId(@NotNull final String id) throws Exception {
        if(id.isEmpty()) throw new Exception("Id project is empty!");
        itaskRepository.deleteAllByProject_ProjectId(id);
    }

    @Override
    public void updateTask(@NotNull final String taskId, @NotNull final String name, @NotNull final String description, @NotNull final Date dateStart,
                           @NotNull final Date dateEnd, @NotNull final Status status) {
        itaskRepository.update(taskId, name, description, dateStart, dateEnd, status);
    }
}
