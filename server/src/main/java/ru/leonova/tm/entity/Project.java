package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public final class Project implements Serializable {

    @Id
    @Column(name = "id")
    private String projectId;
    private String name;
    private String description;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    @Column(name = "createDate")
    private Date dateSystem;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    @Column(name = "beginDate")
    private Date dateStart;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    @Column(name = "endDate")
    private Date dateEnd;

    @Enumerated(EnumType.STRING)
    @Column(name = "statusType")
    private Status status;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.REMOVE)
    private List<Task> tasks;

}
