package ru.leonova.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;

public class ConventDTOUtil {

    public static Project getProject(@NotNull final ProjectDTO projectDTO) throws Exception {
        Project project = new Project();
        project.setProjectId(projectDTO.getProjectId());
        project.setName(projectDTO.getName());
        project.setDateSystem(projectDTO.getDateSystem());
        project.setDateEnd(projectDTO.getDateEnd());
        project.setDateStart(projectDTO.getDateStart());
        project.setStatus(projectDTO.getStatus());
        project.setDescription(projectDTO.getDescription());
        return project;
    }

    public static Task getTask(@NotNull final TaskDTO taskDTO) {
        Task task = new Task();
        task.setName(taskDTO.getName());
        task.setDateSystem(taskDTO.getDateSystem());
        task.setDateEnd(taskDTO.getDateEnd());
        task.setDateStart(taskDTO.getDateStart());
        task.setStatus(taskDTO.getStatus());
        task.setDescription(taskDTO.getDescription());
        task.setTaskId(taskDTO.getTaskId());
        Project project = new Project();
        project.setProjectId(taskDTO.getProjectId());
        task.setProject(project);
        return task;
    }

    public static TaskDTO getTaskDTO(@NotNull final Task task) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTaskId(task.getTaskId());
        taskDTO.setName(task.getName());
        taskDTO.setDateSystem(task.getDateSystem());
        taskDTO.setDateEnd(task.getDateEnd());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setProjectId(task.getProject().getProjectId());
        return taskDTO;
    }

    public static ProjectDTO getProjectDTO(Project project){
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectId(project.getProjectId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateSystem(project.getDateSystem());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateEnd(project.getDateEnd());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

}
