package ru.leonova.tm.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.service.IProjectService;
import ru.leonova.tm.service.ITaskService;
import ru.leonova.tm.util.ConventDTOUtil;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("rest/projects")
public class ProjectRestController {

    @Autowired
    private IProjectService projectService;
    @Autowired
    ITaskService taskService;

    @GetMapping
    public List<ProjectDTO> findAll(){
        List<Project> projects = projectService.findAll();
        List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (Project project:projects) {
            ProjectDTO projectDTO = ConventDTOUtil.getProjectDTO(project);
            projectDTOS.add(projectDTO);
        }
        return projectDTOS;
    }

    @GetMapping("/project/{id}")
    public ProjectDTO getProject(@PathVariable("id") @NotNull final String id) throws Exception {
        Project project = projectService.findOneById(id);
        return ConventDTOUtil.getProjectDTO(project);
    }

    @PostMapping("/create")
    public void create(@RequestBody ProjectDTO projectDTO) throws Exception {
        Project project = ConventDTOUtil.getProject(projectDTO);
        projectService.save(project);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") @NotNull final String id) throws Exception {
        Project project = projectService.findOneById(id);
        taskService.removeAllByProjectId(id);
        projectService.removeProject(project);
    }

    @PutMapping("/update")
    public void update(@RequestBody ProjectDTO projectDTO) {
        projectService.updateProject(projectDTO.getProjectId(), projectDTO.getName(), projectDTO.getDescription(),
                projectDTO.getDateStart(), projectDTO.getDateEnd(), projectDTO.getStatus());
    }

}
