package ru.leonova.tm.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.service.IProjectService;
import ru.leonova.tm.service.ITaskService;
import ru.leonova.tm.util.ConventDTOUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("rest/tasks")
public class TaskRestController {

    @Autowired
    ITaskService taskService;
    @Autowired
    IProjectService projectService;

    private SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");

    @GetMapping
    public List<TaskDTO> findAll(){
        List<Task> tasks = taskService.findAll();
        List<TaskDTO> TaskDTOS = new ArrayList<>();
        for (Task task :tasks) {
            TaskDTO TaskDTO = ConventDTOUtil.getTaskDTO(task);
            TaskDTOS.add(TaskDTO);
        }
        return TaskDTOS;
    }

    @GetMapping("/project/{id}")
    public List<TaskDTO> findAllByProject(@PathVariable @NotNull final String id) throws Exception {
        List<Task> tasks = taskService.findAllByProjectId(id);
        List<TaskDTO> TaskDTOS = new ArrayList<>();
        for (Task task :tasks) {
            TaskDTO TaskDTO = ConventDTOUtil.getTaskDTO(task);
            TaskDTOS.add(TaskDTO);
        }
        return TaskDTOS;
    }

    @GetMapping("/task/{id}")
    public TaskDTO findOne(@PathVariable @NotNull final String id) throws Exception {
        Task task = taskService.findOneById(id);
        @NotNull final TaskDTO TaskDTO = ConventDTOUtil.getTaskDTO(task);
        return TaskDTO;
    }

    @PostMapping("/create")
    public void create(@RequestBody TaskDTO task) throws Exception {
        taskService.save(task.getProjectId(), task.getName(), task.getDescription(), format.format(task.getDateStart()), format.format(task.getDateEnd()));
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable @NotNull final String id) {
        taskService.removeTaskById(id);
    }

    @PutMapping("/update")
    public void update(@RequestBody @NotNull final TaskDTO taskDTO) {
        taskService.updateTask(taskDTO.getTaskId(), taskDTO.getName(), taskDTO.getDescription(), taskDTO.getDateStart(), taskDTO.getDateEnd(), taskDTO.getStatus());
    }

}
