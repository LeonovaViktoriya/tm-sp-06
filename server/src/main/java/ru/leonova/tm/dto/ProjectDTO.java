package ru.leonova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public final class ProjectDTO implements Serializable {

    private  String name;
    @Id
    private String projectId;
    private String description;
    private Date dateSystem;
    private Date dateStart;
    private Date dateEnd;
    private Status status;


}
