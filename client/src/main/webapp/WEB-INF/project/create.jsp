<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <style type="text/css">
        div.form-row{
            margin:10px;
            width: 30%;
        }
        input.btn{
            margin:10px;
            width: 5%;
        }
    </style>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand">Create project</a>
</nav>
<form:form method="POST" action="/create" modelAttribute="projectDTO">
<div class="form-row">
    <label for="name" >Name</label>
    <input type="text" name="name" id="name" class="form-control">
</div>
<div class="form-row">
    <label for="description">Description</label>
    <input type="text" name="description" id="description" class="form-control">
</div>
<div class="form-row">
    <label for="startDate">date Start</label>
    <input type="text" name="startDate" id="startDate" class="form-control">
</div>
<div class="form-row">
    <label for="endDate">date End</label>
    <input type="text" name="endDate" id="endDate" class="form-control">
</div>
    <div class="form-row">
        <label for="status">Выберите status</label>
        <form:select path="status" id="status" name="status" class="form-control">
            <c:forEach items="${statusList}" var="status" >
                <option id="status" name="status" value=${status}>${status}</option>
            </c:forEach>
        </form:select>
    </div>
<input type="submit" value="Add" class="btn btn-success">
</form:form>
</body>
