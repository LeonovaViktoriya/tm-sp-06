<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Tasks</title>
    <style>
        table.table-bordered{
            margin-left: 1%;
            width: 98%;
        }
        a.btn-primary{
            margin-left: 1%;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <h3 class="navbar-brand">List tasks</h3>
    <div class="collapse navbar-collapse" id="navbarText">
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
    <div >
    <a class="navbar-brand" href="/">List projects</a>
    </div>
</nav>
<h3 class="display-3">List tasks for project with id: <br/>${id}</h3>
<br/>
<a class="btn btn-primary" href="/tasks/create/${id}" role="button">Create task</a>
<br/>
<br/>
<table class="table table-bordered">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Project</th>
        <th scope="col">DateStart</th>
        <th scope="col">DateEnd</th>
        <th scope="col">DateSystem</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>${task.taskId}</td>
            <td>${task.name}</td>
            <td>${task.description}</td>
            <td>${task.projectId}</td>
            <td><b:formatDate value="${task.dateStart}" pattern="dd.mm.yyyy"/></td>
            <td><b:formatDate value="${task.dateEnd}" pattern="dd.mm.yyyy"/></td>
            <td><b:formatDate value="${task.dateSystem}" pattern="dd.mm.yyyy"/></td>
            <td>${task.status}</td>
            <td>
                <a href="/tasks/edit/${task.taskId}" type="button" class="btn btn-outline-info">refresh</a>
                <a href="/tasks/delete/${task.taskId}" type="button" class="btn btn-outline-info">delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
