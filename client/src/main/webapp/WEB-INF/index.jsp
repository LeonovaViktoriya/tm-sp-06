<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Index</title>
</head>
<body>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <a class="navbar-brand">Projects and tasks</a>
</nav>
<br/>
<h1>Select table to view!</h1>
<br/>
<a class="btn btn-primary"  href="/projects" role="button">List projects</a>
<a class="btn btn-primary"  href="/tasks" role="button">List tasks</a>
</body>
</html>