package ru.leonova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.leonova.tm.enam.Status;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
public final class ProjectDTO implements Serializable {

    @Id
    private String projectId;
    private  String name;
    private String description;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateSystem;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateStart;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateEnd;
    private Status status;


}
