package ru.leonova.tm.enam;

public enum Status {

    PLANNED("planned"),
    INPROCESS("in-process"),
    READY("ready");

    private String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
