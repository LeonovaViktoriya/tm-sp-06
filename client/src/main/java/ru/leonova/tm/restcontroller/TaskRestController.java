package ru.leonova.tm.restcontroller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.enam.Status;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Getter
@Setter
@Named
@SessionScoped
@Controller
public class TaskRestController {

    private SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");

    @NotNull private final String uri = "http://localhost:8080/rest/tasks";

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String findAll(Model model){
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<TaskDTO[]> responseEntity = restTemplate.getForEntity(uri,
                TaskDTO[].class);
        @Nullable final List<TaskDTO> tasks = Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        model.addAttribute("tasks", tasks);
        return "task/tasks";
    }

    @RequestMapping(value = "/tasks/project/{id}", method = RequestMethod.GET)
    public String findAllForProject(@PathVariable @NotNull final String id, Model model){
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<TaskDTO[]> responseEntity = restTemplate.getForEntity(uri+"/project/"+id,
                TaskDTO[].class);
        @Nullable final List<TaskDTO> tasks = Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        model.addAttribute("tasks", tasks);
        return "task/tasks";
    }

//    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET)
//    public String findOne(@PathVariable @NotNull final String id, Model model)  {
//        @NotNull final RestTemplate restTemplate = new RestTemplate();
//        @Nullable final TaskDTO task = restTemplate.getForObject(uri + "/task/"+ id, TaskDTO.class);
//        @Nullable final List<TaskDTO> tasks = Collections.singletonList(task);
//        model.addAttribute("tasks", tasks);
//        return "task/tasks";
//    }

    @RequestMapping(value = "/tasks/create/{id}", method = RequestMethod.POST)
    public String create(@PathVariable @NotNull String id, @RequestParam String name, @RequestParam String description,
                         @RequestParam String startDate, @RequestParam String endDate, @RequestParam Status status) throws ParseException {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setTaskId(UUID.randomUUID().toString());
        task.setName(name);
        task.setProjectId(id);
        task.setDescription(description);
        task.setDateStart(format.parse(startDate));
        task.setDateEnd(format.parse(endDate));
        task.setDateSystem(new Date());
        task.setStatus(status);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject( uri+"/create", task, TaskDTO.class);
        return "redirect:/tasks/project/{id}";
    }

    @RequestMapping(value = "/tasks/create/{id}", method = RequestMethod.GET)
    public ModelAndView createPage(@PathVariable @NotNull String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(id);
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.addObject(new TaskDTO());
        modelAndView.setViewName("task/create");
        return modelAndView;
    }

    @RequestMapping(value = "/tasks/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") @NotNull final String id) {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(uri+"/delete/"+id,  params);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/tasks/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") @NotNull final String id)  {
        RestTemplate restTemplate = new RestTemplate();
        @Nullable final TaskDTO task = restTemplate.getForObject(uri + "/task/"+ id, TaskDTO.class);;
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("task", Objects.requireNonNull(task));
        modelAndView.setViewName("task/edit");
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        return modelAndView;
    }

    @RequestMapping(value = "/tasks/edit/{id}", method = RequestMethod.POST)
    public String edit(@PathVariable @NotNull final String id, @NotNull final String name, @NotNull final String description,
                       @NotNull final String dateStart, @NotNull final String dateEnd, @RequestParam Status status) throws ParseException {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTaskId(id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setDateStart(format.parse(dateStart));
        taskDTO.setDateEnd(format.parse(dateEnd));
        taskDTO.setStatus(status);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put ( uri+"/update", taskDTO, params);
        return "redirect:/tasks";
    }

}
