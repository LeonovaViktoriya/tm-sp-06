package ru.leonova.tm.restcontroller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.enam.Status;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Getter
@Setter
@Named
@SessionScoped
@Controller
public class ProjectRestController {

    private SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");

    @NotNull private final String uri = "http://localhost:8080/rest/projects";

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(Model model){
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDTO[]> responseEntity = restTemplate.getForEntity(uri,
                ProjectDTO[].class);
        @Nullable final List<ProjectDTO> projects = Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        model.addAttribute("projects", projects);
        return "project/projects";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestParam String name, @RequestParam String description, @RequestParam String startDate,
                         @RequestParam String endDate, @RequestParam Status status) throws ParseException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setProjectId(UUID.randomUUID().toString());
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(format.parse(startDate));
        project.setDateEnd(format.parse(endDate));
        project.setDateSystem(new Date());
        project.setStatus(status);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(uri+"/create", project, ProjectDTO.class);
        return "redirect:";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project/create");
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.addObject(new ProjectDTO());
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") @NotNull final String id) {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(uri+"/delete/"+id,  params);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") @NotNull final String id)  {
        RestTemplate restTemplate = new RestTemplate();
        ProjectDTO project = restTemplate.getForObject( uri+"/project/"+id,ProjectDTO.class);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project/edit");
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.addObject("project", Objects.requireNonNull(project));
        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String update(@PathVariable @NotNull final String id, @NotNull final String name, @NotNull final String description, @NotNull final String dateStart,
                       @NotNull final String dateEnd, @RequestParam Status status) throws ParseException {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectId(id);
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setDateStart(format.parse(dateStart));
        projectDTO.setDateEnd(format.parse(dateEnd));
        projectDTO.setStatus(status);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put ( uri+"/update", projectDTO, params);
        return "redirect:/";
    }

}
